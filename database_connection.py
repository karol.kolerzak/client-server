from sqlite3 import Error


def create_database(connection_pool):
    try:

        connection = connection_pool.getconn()
        cursor = connection.cursor()
        command = '''CREATE TABLE IF NOT EXISTS users (
                                           id SERIAL PRIMARY KEY,
                                           login varchar(50) UNIQUE  NOT NULL,
                                           password varchar(50) NOT NULL,
                                           user_type varchar(10) DEFAULT 'guest'
                                          );
                           '''
        cursor.execute(command)

        command = '''CREATE TABLE IF NOT EXISTS messages(
                                           id SERIAL PRIMARY KEY,
                                           sender varchar(50) NOT NULL,
                                           recipient varchar(50) NOT NULL,
                                           body varchar(255),
                                           is_read bool default False,
                                           CONSTRAINT fk_sender FOREIGN KEY(sender) REFERENCES users(login) ON DELETE CASCADE,
                                           CONSTRAINT fk_recipient FOREIGN KEY(recipient) REFERENCES users(login) ON DELETE CASCADE);
                                   '''
        cursor.execute(command)

        command = "SELECT login FROM users where login = 'admin';"
        cursor.execute(command)
        if cursor.fetchone() is None:
            command = "INSERT INTO users (login, password, user_type) VALUES ('admin', 'admin', 'admin')"
            cursor.execute(command)
            connection.commit()
        info = 'OK. Database exist.'
        reply = (info, True)
        connection_pool.putconn(connection)

    except Error as error:
        reply = (error, False)
    finally:
        return reply

