import socket
import json

SIZE = 64
HOST = "127.0.0.1"
PORT = 6542
FORMAT = 'utf-8'


def send(s, command):
    command = command.encode(FORMAT)
    command_len = str(len(command)).encode(FORMAT)
    command_to_send = command_len + b' ' * (SIZE - len(command_len))
    command_to_send += command
    s.send(command_to_send)
    return recive(s)

def recive(s):
    reply_len = s.recv(SIZE).decode(FORMAT)
    reply_len = int(reply_len)
    reply = s.recv(reply_len).decode(FORMAT)
    # reply = json.loads(answear)
    return reply

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        while True:
            command = input(">>>")
            reply = send(s, command)
            print(reply)
            if 'stop' in reply and len(reply) < 2:
                break

if __name__ == "__main__":
    main()
