import socket
import json
import datetime
from database_connection import create_database
from connection_pool import ConnectionPool
from sqlite3 import Error

SIZE = 64
PORT = 6542
HOST = "127.0.0.1"
FORMAT = 'utf-8'
version = "1.0.0"

# warstwa logiki

current_user = None
connection_pool = ConnectionPool(10, 100)


def execute(query, parameters=()):
    try:
        connection = connection_pool.getconn()
        cursor = connection.cursor()
        cursor.execute(query, parameters)
        reply = cursor.fetchall()
        connection.commit()
        connection_pool.putconn(connection)
    except Error as error:
        info = error
        reply = info
    finally:
        return reply

class User():
    def __init__(self, login, password):
        self.login = login
        self.password = password

    def del_user(self, data):
        login_to_remove = data[0]
        command = "SELECT user_type from users where login=?"
        parameters = (self.login, )
        user_type = (execute(command, parameters))[0][0]
        if user_type == "admin":
            if len(data) == 1:
                if data[0] == 'back':
                    info = ("back")
                    reply = (info, False)
                else:
                    command = "SELECT user_type from users where login=?"
                    parameters = (login_to_remove, )
                    if len(execute(command, parameters)):
                        command = "DELETE from users where login=?;"
                        parameters = (login_to_remove, )
                        execute(command, parameters)
                        info = "User was deleted"
                        reply = (info, False)
                    else:
                        info = "Login is not correct, write>>>[login]  or back"
                        reply = (info, True)
            else:
                info = "Not correct format. write>>>[login] or back"
                reply = (info, True)
        else:
            info = "You don't have access to this options"
            reply = (info, False)

        return reply

    def send_message(self, data):
        if len(data) == 2:
            recipient = data[0]
            message = data[1]
            if len(data[1]) <= 255:
                command = "SELECT user_type FROM users WHERE login = ?;"
                parameters = (recipient, )
                recipient_type = execute(command, parameters)
                print(recipient_type, type(recipient_type))
                if len(recipient_type) != 0:
                    if recipient_type[0][0] != 'admin':
                        command = "SELECT recipient FROM messages WHERE recipient = ? and is_read = 'f';"
                        if len(execute(command, parameters)) < 5:
                            command = "INSERT INTO messages (sender, recipient, body) VALUES (?, ?, ?);"
                            parameters = (self.login, recipient, message)
                            execute(command, parameters)
                            info = "Message has been sent"
                            reply = (info, False)
                        else:
                            info = f"You can't send messege to {recipient}. {recipient} mesbox is fully"
                            reply = (info, False)
                    else:
                        command = "INSERT INTO messages (sender, recipient, body) VALUES (?, ?, ?);"
                        parameters = (self.login, recipient, message)
                        execute(command, parameters)
                        info = "Message has been sent"
                        reply = (info, False)
                else:
                    info = "User does not exist"
                    reply = (info, False)
            else:
                info = "Message is to long. Max 255 characters. write>>>[login]#[message] or back"
                reply = (info, True)
        elif len(data) == 1 and data[0] == 'back':
            info = "back"
            reply = (info, False)
        else:
            info = "Not correct format. write>>>[login]#[message] or back"
            reply = (info, True)

        return reply

    def read_messages(self):
        command = "SELECT sender, body FROM messages WHERE recipient = ? and is_read = 'f';"
        parameters = (self.login, )
        reply = str(execute(command, parameters))
        command = "UPDATE messages SET is_read = 'T' where recipient = ? and is_read = 'f'"
        parameters = (self.login, )
        execute(command, parameters)
        return reply

    def show_all_messages(self):
        command = "SELECT body FROM messages WHERE recipient=?;"
        parameters = (self.login,)
        messages = execute(command, parameters)
        list_of_messages = []
        for message in messages:
            list_of_messages.append(message[0])
        return str(list_of_messages)

    def logout(self):
        global current_user
        reply = f"Logout correctly - {current_user.login}"
        current_user = None
        return reply


#########################################

def login(data):
    if len(data) == 2:
        global current_user
        login = data[0]
        password = data[1]
        user, info = check_user(login, password)
        if user:
            current_user = user
            reply = (info, False)
        else:
            reply = (info, True)
    elif data[0] == 'back' and len(data) == 1:
        info = "back"
        reply = (info, False)
    else:
        info = "Not correct format. write>>>[login] [password] or back"
        reply = (info, True)

    return reply


def check_user(login, password):
    command = "SELECT login, password from users where login=? and password=?;"
    parameters = (login, password)

    if len(execute(command, parameters)):
        reply = f"Login and password are correct. Hello {login}!"
        user = User(login, password)
        return user, reply
    else:
        reply = "Login or password is not correct, write>>>[login] [password]"
        return None, reply

def register(data):
    if data[0] == 'back' and len(data) == 1:
        info = 'back'
        reply = (info, False)
    elif len(data) == 2:
        login = data[0]
        password = data[1]
        command = "SELECT login from users where login=?;"
        parameters = (login, )
        if len(execute(command, parameters)) != 0:
            info = "Login already in use, Choose different login"
            reply = (info, True)
        else:
            command = "INSERT INTO users (login, password) VALUES (?, ?);"
            parameters = (login, password)
            execute(command, parameters)
            info = "New user created"
            reply = (info, False)
    else:
        info = "Not correct format. Try again."
        reply = (info, True)

    return reply



def stop():
    text = {
        "stop": "Server was stop"
    }
    return json.dumps(text, indent=1)


def help():
    text = {
        "uptime": "Return the server uptime",
        "info": "Return version number and date of create the server",
        "help": "Return list of commands with their descriptions",
        "stop": "stopping the server and client",
        "login": "Log in",
        "register": "create new user"
    }
    return json.dumps(text, indent=1)


def uptime(date):
    uptime = str(datetime.datetime.now() - date)
    text = {
        "uptime": uptime
    }
    return json.dumps(text, indent=1)


def info_server(date):
    date = str(date)
    text = {
        "version": version,
        "date": date
    }
    return json.dumps(text, indent=1)


def incorrect_command():
    text = {
        "answear": "Not correct command"
    }
    return json.dumps(text, indent=1)


# warstwa wysyłania

def answear(result):
    print(result)
    result = result.encode(FORMAT)
    len_answear = str(len(result)).encode(FORMAT)
    len_answear += b' ' * (SIZE - len(len_answear))
    return len_answear + result


def send(conn, result):
    message_to_send = answear(result)
    conn.send(message_to_send)


def recive(conn):
    while True:
        message_length = conn.recv(SIZE).decode(FORMAT)
        if message_length:
            message_size = int(message_length)
            message = conn.recv(message_size).decode(FORMAT)
            return message


##############################################################

def handle_client(conn, addr):
    date = datetime.datetime.now()
    while True:
        message_length = conn.recv(SIZE).decode(FORMAT)

        if message_length:
            message_size = int(message_length)
            message = conn.recv(message_size).decode(FORMAT)

            if message == "stop":
                result = stop()
                send(conn, result)
                break
            elif message == "help":
                result = help()
                send(conn, result)
            elif message == "uptime":
                result = uptime(date)
                send(conn, result)
            elif message == "info":
                result = info_server(date)
                send(conn, result)
            elif message == "register":
                info = "write>>>[login] [password] or back"
                status = True
                send(conn, info)

                while status:
                    data = recive(conn)
                    data = data.split()
                    result, status = register(data)
                    send(conn, result)

            elif message == "login":
                info = "write>>>[login] [password] or back"
                status = True
                send(conn, info)

                while (status):
                    data = recive(conn)
                    data = data.split()

                    result, status = login(data)
                    send(conn, result)

            elif message == "logout" and current_user is not None:
                result = current_user.logout()
                send(conn, result)


            elif message == "del_user" and current_user is not None:
                info = "write>>>[login] or back"
                status = True
                send(conn, info)
                while (status):
                    data = recive(conn)
                    data = data.split()

                    result, status = current_user.del_user(data)
                    send(conn, result)

            elif message == "send" and current_user is not None:
                info = "write>>>[login]#[message] or back"
                status = True
                send(conn, info)
                while status:
                    data = recive(conn)
                    data = data.split('#')

                    result, status = current_user.send_message(data)
                    send(conn, result)

            elif message == "read" and current_user is not None:
                result = current_user.read_messages()
                send(conn, result)

            elif message == "all_mes" and current_user is not None:
                result = current_user.show_all_messages()
                send(conn, result)

            else:
                result = incorrect_command()
                send(conn, result)


#########################################

def main():

    reply, status = create_database(connection_pool)
    print(reply)
    if status:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            s.listen()
            while True:
                conn, addr = s.accept()
                handle_client(conn, addr)


if __name__ == "__main__":
    main()
