import unittest
import server

class TestUser(unittest.TestCase):

    def test_register(self):
        data = [('test123', 'test123'), ('test', 'test'), ('test123', 'testerror'), ('abc', 'abc', 'abc')]
        correct_answear = [("New user created", False), ("New user created", False), ("Login already in use, Choose different login", True), ("Not correct format. Try again.", True)]
        for x in range(len(data)):
            result = server.register(data[x])
            self.assertEqual(result, correct_answear[x])

    def test_del_user(self):
        server.register(["test123", "test123"])
        server.login(["test", "test"])
        user = server.current_user
        result = user.del_user(['test123'])
        self.assertEqual(result, ("You don't have access to this options", False))
        user.logout()

        server.register(["test123", "test123"])
        server.login(["admin", "admin"])
        user = server.current_user
        result = user.del_user(['test123'])
        self.assertEqual(result, ("User was deleted", False))
        user.logout()

    def test_login(self):
        users = [('admin', 'admin'), ("admin", "admin1"), ("admin", "admin", "admin", "admin")]
        answear = [(f"Login and password are correct. Hello admin!", False), ("Login or password is not correct, write>>>[login] [password]", True), ("Not correct format. write>>>[login] [password] or back", True)]
        for x in range(len(users)):
            result = server.login(users[x])
            self.assertEqual(result, answear[x])

    def test_sending_message(self):
        server.login(["admin", "admin"])
        user = server.current_user
        data = [("test", "test"), ("test", "*" * 256)]
        answears = [("Message has been sent", False), ("Message is to long. Max 255 characters. write>>>[login]#[message] or back", True)]
        for x in range(len(data)):
            result = user.send_message(data[x])
            self.assertEqual(result, answears[x])

    def test_mesbox_limit(self):
        server.register(["test123", "test123"])
        server.login(["admin", "admin"])
        user = server.current_user

        for x in range(5):
            result = user.send_message(["test123", "test"])
            self.assertEqual(result, ("Message has been sent", False))

        result = user.send_message(["test123", "test"])
        self.assertEqual(result, ("You can't send messege to test123. test123 mesbox is fully", False))




if __name__ == '__main__':
    unittest.main()
